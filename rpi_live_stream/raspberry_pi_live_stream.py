import picamera
from output import output
from server import Server
from handler import Handler


if __name__ == "__main__":
    """Main function responsible for enabling Pi Camera live streaming
    """
    camera = picamera.PiCamera(resolution='640x480', framerate=24)
    camera.rotation = 180
    camera.start_recording(output, format='mjpeg')
    try:
        address = ('', 8000)
        server = Server(address, Handler)
        server.serve_forever()
    finally:
        camera.stop_recording()
