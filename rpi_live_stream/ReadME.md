## Raspberry Pi live camera stream

File  *raspberry_pi_live_stream.py* contains code responsible for starting live camera stream. In order to start the camera live stream, start live camera service:
```
python3 -B raspberry_pi_live_stream.py
```
Option `-B` disables creating unnecessary __pycache__ folder

Then using Raspberry Pi IP address go to the web browser and type: 
```
http://192.168.0.21:8000/
``` 
assuming that *192.168.0.21* is a local Raspberry Pi IP address. If the server is running properly, under this website there will be a live camera stream available.
