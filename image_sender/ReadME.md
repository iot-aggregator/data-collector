## Raspberry Pi image sender

When a MinIO service is running, taken photos can be sent from Raspberry Pi in given time interval.

More information about MinIO: <https://docs.min.io/docs/>
