from abc import ABC, abstractmethod


class FileSender(ABC):
    def __init__(self, client):
        super().__init__()
        self.client = client

    @abstractmethod
    def sendFile(self, destination, file_path, object_name=None):
        pass
