from abc import ABC
import logging
from minio import Minio, S3Error
from image_sender.file_sender import FileSender
from utils.yaml_converter import get_config_as_dict


class S3FileSender(FileSender, ABC):

    def __init__(self, endpoint, access_key=get_config_as_dict()['services']['minio']['access_key'],
                 secret_key=get_config_as_dict()['services']['minio']['secret_key'], secure=False):
        super().__init__(Minio(endpoint, access_key=access_key,
                               secret_key=secret_key, secure=secure))

    def sendFile(self, bucket_name, bucket_object_name, local_object_path):
        try:
            # Make sure bucket exists
            if not self.client.bucket_exists(bucket_name):
                logging.info(f'Bucket does not exist. Creating one ({bucket_name})')
                self.client.make_bucket(bucket_name)

            # Send image to the MinIO server
            self.client.fput_object(bucket_name=bucket_name, object_name=bucket_object_name,
                                    file_path=local_object_path)

            try:    # Check if file was not corrupted while sending
                response = self.client.get_object(bucket_name, bucket_object_name)
                image_file = open(local_object_path, "rb")
                if response.data != image_file.read():
                    logging.error('Sent file got corrupted')
            finally:
                image_file.close()
                response.close()
                response.release_conn()
                logging.info('Image sent and delivered correctly')

        except S3Error as ex:
            logging.error(f'A following error occured while sending image: {ex}')
