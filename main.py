from schedulers.measurements.analog_input.measurement_thread \
    import MeasurementThread
from schedulers.photographs.photographs_thread import PhotographsThread
from schedulers.measurements.voltage_output.voltage_scheduler \
    import VoltageScheduler
from utils.force_exit_handler import ForceExitHandler
from utils.functions import init_raspberry_pi
from utils.logger import logger_setup
import logging


if __name__ == "__main__":
    """Main function
    """
    # Set up handler for force exit interrupt
    ForceExitHandler().start()

    # Set up logger
    logger_setup()

    # RaspberryPi initial set up
    logging.info('Initialize Raspberry Pi configuration ...')
    init_raspberry_pi()

    # Schedule sending measurements
    MeasurementThread().start()

    # Schedule sending photographs
    PhotographsThread().start()

    # Schedule voltage controllers actions
    VoltageScheduler().schedule_actions()
