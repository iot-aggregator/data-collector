## Flask REST api

REST api created in Python framework Flask enables to serve few endpoint in order to get and update Raspberry Pi configuration.

Raspberry Pi configuration endpoint:
```
http://192.168.0.21:5000/config
``` 
assuming *192.168.0.21* is a local Raspberry Pi IP address.


__GET__ request - returns current local config file content

__POST__ request - enables to update configuration with request body matching config template
