import serial

# Implement handling of Red-y gas flow controller

ComPort = serial.Serial('COM3') # open COM3
ComPort.baudrate = 9600         # set Baud rate to 9600
ComPort.bytesize = 8            # Number of data bits = 8
ComPort.parity   = 'N'          # No parity
ComPort.stopbits = 1            # Number of Stop bits = 1
