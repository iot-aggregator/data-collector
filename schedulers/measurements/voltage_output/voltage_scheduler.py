import RPi.GPIO as GPIO
from schedulers.measurements.voltage_output.voltage_controller_thread \
    import VoltageControllerThread
from utils.yaml_converter import get_config_as_dict


class VoltageScheduler():
    """Class for scheduling voltage management on output pins
    """

    def schedule_actions(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        for controller_no in list(range(get_config_as_dict()['controller_max_number'])):
            VoltageControllerThread(controller_no).start()
