import time
import logging
import threading
import RPi.GPIO as GPIO
from utils.yaml_converter import get_config_as_dict
from utils.functions import update_raspberry_config


class VoltageControllerThread(threading.Thread):
    """Class for performing voltage sending to all output pins separately

    Args:
        threading (Thread): Enables to inherit from class 'Thread'
    """

    def __init__(self, controller_no):
        self.controller_no = controller_no
        logging.info(f'Start thread for controller number {controller_no}')
        threading.Thread.__init__(self)

    def run(self) -> None:
        while True:
            # Perform pin output only when pin is active
            while self.if_output_pin_active():
                # Set GPIO number for this controller
                GPIO.setup(self.gpio, GPIO.OUT)
                GPIO.output(self.gpio, GPIO.LOW)

                # Wait requested amount of time
                time.sleep(self.waiting_time)

                # Turn on action
                GPIO.output(self.gpio, GPIO.HIGH)
                logging.info(f'GPIO{self.gpio} (controller {self.controller_no}) set to HIGH')

                # Wait requested amount of time
                time.sleep(self.action_time)

                # Turn off action
                GPIO.output(self.gpio, GPIO.LOW)
                logging.info(f'GPIO{self.gpio} (controller {self.controller_no}) set to LOW')

    def update_configuration(self):
        controllers =  get_config_as_dict('config_raspberry.yml')['config']['controllers']
        if self.controller_no < len(controllers):
            self.action_time = int(controllers[self.controller_no]['actionTimeInSeconds'])
            self.waiting_time = int(controllers[self.controller_no]['waitingTimeInSeconds'])
            self.gpio = int(controllers[self.controller_no]['pinName'].replace('GPIO', ''))
        else:
            self.action_time = -1
            self.waiting_time = -1
            self.gpio = -1
            time.sleep(60)
            update_raspberry_config()

    def if_output_pin_active(self):
        self.update_configuration()
        if self.gpio == -1:
            return False
        if self.action_time == -1 or self.waiting_time == -1:
            return False
        else:
            return True
