import busio
import board
import digitalio
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
from schedulers.measurements.analog_input.measurement import Measurement
from utils.yaml_converter import get_config_as_dict


# RaspberryPi pins setup
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
cs = digitalio.DigitalInOut(board.D14)
mcp_converter = MCP.MCP3008(spi, cs)

# Input pins
pin_0 = AnalogIn(mcp_converter, MCP.P0)
pin_1 = AnalogIn(mcp_converter, MCP.P1)
pin_2 = AnalogIn(mcp_converter, MCP.P2)
pin_3 = AnalogIn(mcp_converter, MCP.P3)
pin_4 = AnalogIn(mcp_converter, MCP.P4)
pin_5 = AnalogIn(mcp_converter, MCP.P5)
pin_6 = AnalogIn(mcp_converter, MCP.P6)
pin_7 = AnalogIn(mcp_converter, MCP.P7)


def get_pin_value(converter_pin_no: int) -> int:
    """Get value of requested pin with analog input

    Args:
        channel_no (integer): Number of requested analog pin

    Returns:
        integer: Digital value of requested pin with analog input
    """
    try:
        return globals()[f'pin_{converter_pin_no}'].value
    except: 
        logging.error(f'Reading from analog pin {converter_pin_no} out of range!')


def get_active_pins_converted_values() -> list:
    """Get values of all active analog pins, get values of those pins and
    convert them into Measurement object

    Returns:
        list: list of all active analog pins values converted into
                Measurement object
    """
    sensors = get_config_as_dict('config_raspberry.yml')['config']['sensors']
    pins = [int(s['pinName'].replace('ANALOG', '')) for s in sensors]
    linear_config_coeff_a = [int(s['coefficientA']) for s in sensors]
    linear_config_coeff_b = [int(s['coefficientB']) for s in sensors]

    return [Measurement(pin_no=pins[i], value=get_pin_value(pins[i]) * \
        linear_config_coeff_a[i] + linear_config_coeff_b[i]) \
        for i in range(len(sensors))]
