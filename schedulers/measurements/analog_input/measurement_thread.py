import time
import pika
import os
import logging
import threading
from schedulers.measurements.analog_input.adc_converter \
    import get_active_pins_converted_values
from utils.yaml_converter import get_config_as_dict
from utils.rabbitmq_connection import RabbitMQConnection
from utils.functions import update_raspberry_config


class MeasurementThread(threading.Thread):
    """Class for scheduling and performing data measurement from sensors

    Args:
        threading (Thread): Enables to inherit from class 'Thread'
    """

    def __init__(self):
        threading.Thread.__init__(self)

    def run(self) -> None:
        """Method run when a thread is created. Gets active pins values
        in requested time intervals and sends results to the external server.
        """
        while True:
            # Wait requested amount of time
            time.sleep(get_config_as_dict()['measurement_interval'])

            # Get measurements from all active pins
            measurement_results = get_active_pins_converted_values()

            # Send results to the server
            try:
                for result in measurement_results:
                    RabbitMQConnection().send_message(result.as_dict())
                    update_raspberry_config()
                logging.info('All measurements sent to the server')
            except:
                logging.error('Error while sending measurements to the server')
