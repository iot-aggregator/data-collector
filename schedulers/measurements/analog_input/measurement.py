from datetime import datetime
from utils.yaml_converter import get_config_as_dict
import json


class Measurement():
    """Class responsible for representing a single measurement
    """
    def __init__(self, pin_no, value):
        self.InstanceId = get_config_as_dict()['raspberry_pi']['guid']
        self.MeasuredAt = datetime.now().strftime(get_config_as_dict()['datetime_format'])
        self.PinName = f'ANALOG{pin_no}'
        self.measuredValue = value
        self.tags = {"abd": "Def"}

    def __str__(self):
        return f'{self.pin_no} = {self.value}'

    def as_dict(self) -> str:
        return self.__dict__
