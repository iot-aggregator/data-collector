
Class *Camera* enables taking pictures using Python library called *picamera*. The photography is saved with the title describing when and at what time it was taken using ```ISO-8601``` datetime format template. 

In order to take a photo execute the following code:
```
from camera import Camera

if __name__ == "__main__":
    cam = Camera()
    cam.take_picture()
```
