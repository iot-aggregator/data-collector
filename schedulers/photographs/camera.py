import os
import logging
import pathlib
import schedulers.photographs.camera_config as camera_config
from picamera import PiCamera
from datetime import datetime
from utils.yaml_converter import get_config_as_dict


class Camera:
    def __init__(self):
        self.cam = PiCamera()
        self.cam.rotation = camera_config.rotation
        self.cam.resolution = camera_config.resolution
        self.cam.framerate = camera_config.framerate
        self.cam.brightness = camera_config.brightness
        self.cam.contrast = camera_config.contrast
        self.cam.color_effects = camera_config.color_effects
        self.cam.saturation = camera_config.saturation
        self.cam.sharpness = camera_config.sharpness

        # Photo time
        self.photo_time = datetime.now() #.strftime(get_config_as_dict()['datetime_format'])
        self.photo_year = self.photo_time.year
        self.photo_month = self.photo_time.month
        self.photo_day = self.photo_time.day
        self.photo_hour = self.photo_time.hour
        self.photo_min = self.photo_time.minute

        self.dir = f'{pathlib.Path().absolute()}'
        self.photo_name = f'{self.photo_time.isoformat()}.jpg'
        self.photo_absolute_dir = f"{self.dir}/{get_config_as_dict()['raspberry_pi']['guid']}/" \
            f"{self.photo_year}/{self.photo_month}/{self.photo_day}/{self.photo_time.isoformat()}"
        self.photo_dir = f"{get_config_as_dict()['raspberry_pi']['guid']}/" \
            f"{self.photo_year}/{self.photo_month}/{self.photo_day}/{self.photo_time.isoformat()}/{self.photo_name}"
        self.photo_path = f'{self.photo_absolute_dir}/{self.photo_name}'
        self.check_dir_existence()

    def get_photo_name(self) -> str:
        return self.photo_name

    def get_photo_path(self) -> str:
        return self.photo_path
    
    def get_photo_local_dir(self) -> str:
        return self.photo_dir

    def check_dir_existence(self) -> None:
        if not pathlib.Path(self.photo_absolute_dir).exists():
            os.makedirs(self.photo_absolute_dir, exist_ok=True)

    def check_photo_existence(self) -> bool:
        if os.path.isfile(pathlib.Path(self.photo_path)):
            return True
        else:
            logging.error('Error while taking photo')
            return False

    def take_picture(self) -> None:
        self.cam.capture(self.photo_path)

    def close_camera(self) -> None:
        self.cam.close()

    def delete_picture(self) -> None:
        if self.check_photo_existence():
            os.remove(self.photo_path)
