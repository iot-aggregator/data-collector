import time
import threading
import logging
from schedulers.photographs.camera import Camera
from utils.yaml_converter import get_config_as_dict
from image_sender.s3_file_sender import S3FileSender


class PhotographsThread(threading.Thread):
    """Class for scheduling and performing photos taking

    Args:
        threading (Thread): Enables to inherit from class 'Thread'
    """
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self) -> None:
        """Method run when a thread is created. Initializes PiCamera,
        takes photo in requested time intervals and sends results to the
        external server.
        """
        while True:
            # Wait requested amount of time
            time.sleep(get_config_as_dict()['photography_interval'])

            # Take picture
            cam = Camera()
            cam.take_picture()
            cam.close_camera()

            # Send result to the server
            if cam.check_photo_existence():
                sender = S3FileSender(get_config_as_dict()['services']['minio']['url'])
                sender.sendFile(bucket_name=get_config_as_dict()['services']['minio']['bucket'],
                                bucket_object_name=cam.get_photo_local_dir(), local_object_path=cam.get_photo_path())
                cam.delete_picture()
