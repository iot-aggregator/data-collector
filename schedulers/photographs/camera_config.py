

"""Camera configuration

    dest_path (pathlib.Path): path to images directory
    rotation (integer): camera image rotation; range <0, 360>
    resolution (tuple): camera resolution
    framerate (integer): camera framerate; range <0, 100>
    brightness (integer): camera brightness; range <0, 100>
    contrast (integer): camera image contrast; range <-100, 100>
    color_effects (integer): range (<0,255>, <0,255>)
    saturation (integer): camera saturation; range <-100, 100>
    sharpness (integer): image sharpness; range <-100, 100>
    """

rotation = 180
resolution = (2592, 1944)
framerate = 15
brightness = 55
contrast = 5
color_effects = None
saturation = 10
sharpness = 80
