from flask import Flask, request, jsonify
from utils.functions import init_raspberry_pi
from utils.yaml_converter import get_config_as_dict
from utils.logger import logger_setup


app = Flask(__name__)


@app.route('/config', methods=['GET'])
def get_config():
    if request.method == "GET":
        return {"config": get_config_as_dict()}


@app.route('/config', methods=['POST'])
def post():
    init_raspberry_pi()
    return jsonify({"response_code": 200})


if __name__ == "__main__":
    logger_setup()
    app.run(debug=True, host='0.0.0.0', port=5000)  # turn off when done
