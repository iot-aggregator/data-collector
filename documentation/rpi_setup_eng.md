# Raspberry Pi setup schema - hardware

The below schema presents the Raspberry Pi hardware setup. 

<img src="documentation/RPi_with_MCP3008_schema.PNG" width="600">

# Schema components:
## Raspberry Pi
* pins layout is an inseparable element of each Raspberry Pi device and looks as following:

    <img src="documentation/Raspberry-Pi-GPIO.png" width="400">

## MCP3008 
8-channel 10-bit analog to digital converter. 

<img src="documentation/MCP3008_schema.PNG" width="250">

Necessary thing is to connect all legs (on the right side looking forward) of the MCP3008 chip correctly according to their names and purpose as described below:

- MCP3008 VDD <-> Raspberry Pi (3V3 Power)
- MCP3008 VREF <-> Raspberry Pi (3V3 Power)
- MCP3008 AGND <-> Raspberry Pi (Ground)
- MCP3008 CLK <-> Raspberry Pi (GPIO11)
- MCP3008 DOUT <-> Raspberry Pi (GPIO9)
- MCP3008 DIN <-> Raspberry Pi (GPIO10)
- MCP3008 CS/SHDN <-> Raspberry Pi (GPIO14)
- MCP3008 DGND <-> Raspberry Pi (Ground)

The left-sided legs are enabled as input channels which can be used as analog sensors output data pins.


## More information
- Raspberry Pi 4: <https://www.raspberrypi.org/documentation/computers/getting-started.html>
- MCP3008: <https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/mcp3008>
