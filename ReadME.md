# Data collector

Repository ***data-collector*** is a part of the engineer project realized at the Gdansk University of Technology. This repository contains code and programs which will be executed on the Raspberry Pi device. 

### Camera module
- taking photographs
- sending taken photos to the MinIO service
- observing live camera stream within the local network

### Measurements module
- take measurements using threads
- sending measured values to RabbitMQ

### Logger
- logging results to the log file
