import yaml


def get_config_as_dict(file_name='config.yml'):
    with open(file_name, 'r') as ymlfile:
        return yaml.safe_load(ymlfile)


def update_config(file_name='config.yml'):
    config = get_config_as_dict()
    config['ip_addresses']['web_api'] = '123.123.123.123'

    with open(file_name, 'w') as ymlfile:
        yaml.dump(config, ymlfile)
