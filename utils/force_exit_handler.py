import signal
import logging
import RPi.GPIO as GPIO


class ForceExitHandler():
    """Class for handling envirnment clean up after unexpected program kill
    """
    def __init__(self):
        pass

    def output_pins_cleanup(self):
        """ Make sure all outpus pins will be turned off
        """
        logging.info("Output pins clean up ...")
        GPIO.cleanup()

    def inform_web_about_force_exit(self):
        """Inform integration service that Raspberry experienced force exit
        """
        # TODO
        logging.info("Sending information to WEB about force exit ...")

    def force_exit_handler(self, signum, frame):
        """Handler for force exit
        """
        self.output_pins_cleanup()
        self.inform_web_about_force_exit()
        logging.info('Program force exit after receiving (SIGTERM)')
        exit(0)

    def start(self):
        signal.signal(signal.SIGTERM, self.force_exit_handler)
