import logging
from utils.yaml_converter import get_config_as_dict
import requests
import json
import yaml
import sys


def init_raspberry_pi() -> None:
    """Initialize Raspberry Pi configuration"""

    set_cmdline_parameters()
    update_raspberry_config()


def set_cmdline_parameters() -> None:
    config = get_config_as_dict()

    guid = sys.argv[1]
    measurement_interval = int(sys.argv[2])
    photography_interval = int(sys.argv[3])

    config['raspberry_pi']['guid'] = guid
    config['measurement_interval'] = measurement_interval
    config['photography_interval'] = photography_interval

    with open('config.yml', 'w') as outfile:
        yaml.dump(config, outfile, default_flow_style=False)


def update_raspberry_config() -> None:
    config = get_config_as_dict()

    url = f"{config['services']['integration_service']['raspberry_pi_config']}" \
        f"{config['raspberry_pi']['guid']}"
    rpi_config = requests.get(url=url).json()

    with open('config_raspberry.yml', 'w') as outfile:
        yaml.dump(rpi_config, outfile, default_flow_style=False)
