import os
import pathlib
import logging
from datetime import datetime
from utils.yaml_converter import get_config_as_dict


def logger_setup() -> None:
    """Set up logger in order to save all log above requested level to the external
    log file with given logs format.

    Args:
        None

    Returns:
        None
    """
    logs_dir = pathlib.Path(f"{pathlib.Path().absolute()}/{get_config_as_dict()['logs_dir_name']}")
    if not logs_dir.exists():
        os.mkdir(logs_dir)

    start_date = datetime.now().strftime(get_config_as_dict()['datetime_format'])
    logging.basicConfig(filename=f'logs/log_start_time_{start_date}.log', level=logging.INFO,
                        format='[%(levelname)s] [%(asctime)s] %(message)s')
    logging.info('Program start')
