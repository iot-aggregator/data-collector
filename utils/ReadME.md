## Utils

#### This folder contatins bunch of useful code pieces such as:
`config.py` - local configuration file which mirrors the configuration set up from Integration Service

`force_exit_handler.py` - handler for force exit (i.e. using Crtl+C) while python program is running

`functions.py` (maybe will change name later) - Raspberry Pi set up preparation

`logger.py` - setup file for logger

`tox.ini` - Flake8 specified configuration file
 