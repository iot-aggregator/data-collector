import pika
import json
import logging
from utils.yaml_converter import get_config_as_dict


class RabbitMQConnection():
    def __init__(self):
        self.credentials = pika.PlainCredentials(
            get_config_as_dict()['services']['rabbit_mq']['user'],
            get_config_as_dict()['services']['rabbit_mq']['pwd'])
        self.parameters = pika.ConnectionParameters(
            get_config_as_dict()['services']['rabbit_mq']['url'],
            get_config_as_dict()['services']['rabbit_mq']['port'], '/', self.credentials)

        self.connection = pika.BlockingConnection(self.parameters)
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue='pin-measured', durable=True)

    def send_message(self, message):
        # Pack message in expected JSON format
        msg_type = [get_config_as_dict()['services']['rabbit_mq']['msg_type']]
        response = {}
        response['message'] = message
        response['messageType'] = msg_type
        result = json.dumps(response, indent=4)

        # Send message
        self.channel.confirm_delivery()
        self.channel.basic_publish(
            exchange='',
            routing_key='pin-measured',
            body=result,
            properties=pika.BasicProperties(delivery_mode=2))


    def close_connection(self):
        self.connection.close()
